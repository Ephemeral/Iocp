# disable default implicit rules
MAKEFLAGS += --no-builtin-rules
.SUFFIXES:

# project name
PROJECT  = IocpTest.exe

# common compiler/linker flags
CXX      = g++
CXXFLAGS = -std=c++11 -s -fno-rtti -Wall -Wextra -Wpedantic -Wstrict-aliasing=2
LDFLAGS  = -static -static-libgcc
DEFINES  =

# build specific compiler/linker flags
BUILD ?= release
ifeq ($(BUILD), debug)
  CXXFLAGS += -Og -g
  DEFINES  += -DVERBOSE=1
else ifeq ($(BUILD), profile)
  CXXFLAGS += -march=native -O3 -g
  DEFINES  += -DNDEBUG
else ifeq ($(BUILD), release)
  CXXFLAGS += -fstrict-aliasing -O3 -flto
  LDFLAGS  += -fuse-linker-plugin -s -flto
  DEFINES  += -DNDEBUG
else
  $(error unrecognized BUILD)
endif

BUILDDIR = bin/$(BUILD)
SRCDIR = src
LIBDIR = lib

# generate lists of object files (all and existing only)
SOURCES_ALL := $(wildcard $(SRCDIR)/*.cpp)
OBJECTS_ALL := $(addprefix $(BUILDDIR)/, $(patsubst %.cpp, %.o, $(notdir $(SOURCES_ALL))))
  
OBJECTS_EXT := $(wildcard $(BUILDDIR)/*.o)

# generate lists of libraries
LIBRARIES_ALL := $(wildcard $(LIBDIR)/*.a)
LIBRARIES_LIST := $(addprefix -l, $(patsubst %.a, %, $(patsubst lib%, %, $(notdir $(LIBRARIES_ALL)))))

# main executable
$(BUILDDIR)/$(PROJECT) : $(OBJECTS_ALL) | $(BUILDDIR)
	$(CXX) $(CXXFLAGS) $^ $(LDFLAGS) -o $@ -L./$(LIBDIR) $(LIBRARIES_LIST)

# define static explicit rule to compile a cpp file
$(OBJECTS_ALL): $(BUILDDIR)/%.o : $(SRCDIR)/%.cpp $(SOURCES_ALL) | $(BUILDDIR)
	$(CXX) $(CXXFLAGS) $(DEFINES) -c $< -o $@
	$(CXX) $(CXXFLAGS) $(DEFINES) -MM $< -MT $@ > $(BUILDDIR)/$*.dep

# build directory creation
$(BUILDDIR):
	mkdir -p $(BUILDDIR)

# pull in dependency info for existing object files
-include $(OBJECTS_EXT:.o=.dep)

# obligatory clean target
clean:
	rm -rf ./bin/*