#ifndef _OVERLAPPED_H_
#define _OVERLAPPED_H_
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <WS2tcpip.h>
#include <stdint.h>
#include "NetworkCallback.h"
class OverlappedExtended : public OVERLAPPED
{
public:
	enum OverlappedOperation
	{
		Accept = 1,
		Read,
		Write
	};
	SOCKET Socket;
	uint8_t* Buffer;
	uint32_t Size;
	uint32_t Offset;
	int8_t CompleteOnAll;
	uint64_t InternalExtra;
	uint64_t ExternalExtra;
	//Connection* Self;
	OverlappedOperation Operation;

	LPVOID CompletionRoutine;
	LPVOID NetworkInstance;
	
	OverlappedExtended(/*Connection* Self,*/ SOCKET Socket,
		LPVOID NetworkInstance,
		OverlappedOperation Operation,
		uint8_t* Buffer,
		uint32_t Size,
		LPVOID CompletionRoutine,
		uint64_t InternalExtra,
		uint64_t ExternalExtra,
		int8_t CompleteOnAll);

	~OverlappedExtended();
	LPOVERLAPPED GetOverlapped();
	VOID CleanOverlapped();
};

#endif