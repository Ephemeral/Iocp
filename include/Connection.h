#ifndef _CONNECTION_H_
#define _CONNECTION_H_
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <WS2tcpip.h>
#include <stdint.h>


#if _DEBUG
#include <stdio.h>
#endif


class Connection
{
public:
	virtual SOCKET GetSocket() = 0;
	/*
	{
		return Socket;
	}
	*/

	virtual Connection* GetKey() = 0;
	/*
	{
		return this;
	}
	*/

};
#endif