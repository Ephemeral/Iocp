#ifndef _EXCEPTION_H_
#define _EXCEPTION_H_

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <stdint.h>

#define STATUS_NETWORK_INIT_FAILED					((uint32_t)0xD0000001L)
#define EXCEPTION_FAILED_NETWORK_INITIALIZATION		STATUS_NETWORK_INIT_FAILED

class Exception
{
public:
	static VOID RaiseNetworkException();
};
#endif