#ifndef _NETWORK_H_
#define _NETWORK_H_

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <WS2tcpip.h>
#include <MSWSock.h>

#include <stdint.h>

#include "Overlapped.h"

#include "Exception.h"


#if _DEBUG
#include <stdio.h>
#endif



#if _MSC_VER
	#pragma comment(lib,"WS2_32")
	#pragma comment(lib,"Mswsock.lib")
#endif

class Network
{
private:
	struct ThreadBlock
	{
		HANDLE ThreadHandle;
		ULONG ThreadId;
	};
	WSADATA Wsa;
	ADDRINFO* Result;
	ThreadBlock CompletionThreads[8];
	HANDLE CompletionPort;
public:
	Network();
	~Network();
	
	int8_t InitializeNetwork(int8_t* Server, int8_t* Port);
	int8_t Accept(SOCKET Socket, NetworkAcceptExCompletionRoutine CompletionRoutine,uint64_t ExtraForCallback);
	int8_t Connect(SOCKET Socket);
	int8_t Read(SOCKET Socket,uint8_t* Buffer,uint32_t SizeToRead,NetworkReadWriteCompletionRoutine CompletionRoutine,uint64_t ExtraForCallback,int8_t CompleteOnAll);
	int8_t Write(SOCKET Socket,uint8_t* Buffer,uint32_t SizeToWrite,NetworkReadWriteCompletionRoutine CompletionRoutine,uint64_t ExtraForCallback,int8_t CompleteOnAll);

	SOCKET CreateSocket();
private:
	HANDLE GetCompletionPort();

	LPFN_ACCEPTEX InitializeAcceptEx(SOCKET Socket);
	LPFN_GETACCEPTEXSOCKADDRS InitializeGetacceptExSockAddrs(SOCKET Socket);
	LPFN_CONNECTEX InitializeConnectEx(SOCKET Socket);

	HANDLE AssociateCompletionPort(HANDLE Port,HANDLE ExistingPort,uint64_t Key);
	
	int8_t StartWsa();
	int8_t CreateAddressResult(int8_t* Server,int8_t* Port);
	int8_t CreateCompletionThreads();
	int8_t ReadWriteInternal(OverlappedExtended* Overlapped);
	int8_t ReadWriteInternal(OverlappedExtended* Overlapped,LPWSABUF WsaBuffer);

	VOID WsaCompletionRoutine(ULONG Error,ULONG Transferred,LPWSAOVERLAPPED Overlapped);
	VOID HandleAcceptInternal(uint64_t Key,OverlappedExtended* Overlapped,uint32_t SizeOfCompletion);
	VOID DestroyCompletionTrheads();
	
	static uint32_t CALLBACK CompletionThread(LPVOID Context);
	
	
};
#endif