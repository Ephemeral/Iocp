#ifndef _NETWORK_CALLBACK_H_
#define _NETWORK_CALLBACK_H_
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <WS2tcpip.h>
#include <stdint.h>
#include "Connection.h"



typedef VOID (__cdecl* NetworkReadWriteCompletionRoutine)
	(
	SOCKET Socket,
	uint32_t Error,
	uint8_t* Buffer,
	uint32_t TotalBufferSize,
	uint32_t Transferred,
	uint64_t ExtraForCallback
	);




typedef VOID (__cdecl* NetworkAcceptExCompletionRoutine)(SOCKET AcceptedSocket,
														 LPSOCKADDR ClientAddressLocal,
														 int32_t ClientAddressLocalSize,
														 LPSOCKADDR ClientAddressRemote,
														 int32_t ClientAddressRemoteSize,
														 uint64_t ExtraForCallback);

#endif