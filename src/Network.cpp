#include "..\\include\\Network.h"
uint32_t CALLBACK CompletionThread(LPVOID Context);
VOID CALLBACK WsaCompletionRoutine(ULONG Error,ULONG Transferred,LPWSAOVERLAPPED Overlapped,ULONG Flags);

Network::Network()
{
	Result = NULL;

	RtlSecureZeroMemory(&Wsa,sizeof(WSADATA));

	RtlSecureZeroMemory(CompletionThreads,sizeof(CompletionThreads));

}

Network::~Network()
{

#if _DEBUG
	printf("Network destructor called.\n");
#endif

	if(Result)
	{
		freeaddrinfo(Result);
		Result = NULL;
	}

	DestroyCompletionTrheads();

	if(CompletionPort)
	{
		CloseHandle(CompletionPort);
		CompletionPort = NULL;
	}
	WSACleanup();
}
int8_t Network::InitializeNetwork(int8_t* Server,int8_t* Port)
{
	if(!StartWsa())
		return 0;

	if(!CreateAddressResult(Server,Port))
		return 0;

	CompletionPort = AssociateCompletionPort(INVALID_HANDLE_VALUE,NULL,0);

	if(!CompletionPort)
		return 0;


	if(!CreateCompletionThreads())
		return 0;

	return 1;
}

int8_t Network::StartWsa()
{
	if(!WSAStartup(0x202,&Wsa))
		return 1;

	return 0;
}

int8_t Network::CreateAddressResult(int8_t* Server,int8_t* Port)
{
	ADDRINFO Information = {0};
	Information.ai_family = AF_INET;
	Information.ai_protocol = IPPROTO_TCP;
	Information.ai_socktype = SOCK_STREAM;
	Information.ai_flags = AI_PASSIVE;

	if(!getaddrinfo((LPCSTR)Server,(LPCSTR)Port,&Information,&Result))
		return 1;

	return 0;

}

SOCKET Network::CreateSocket()
{
	SOCKET Socket = INVALID_SOCKET;

	if(!Result)
		return Socket;

	//Socket = WSASocketW(Result->ai_family,Result->ai_socktype,Result->ai_protocol,NULL,0,WSA_FLAG_OVERLAPPED);
	Socket = socket(Result->ai_family,Result->ai_socktype,Result->ai_protocol);
	if(Socket == INVALID_SOCKET)
		return Socket;

	if(AssociateCompletionPort((HANDLE)Socket,CompletionPort,(uint64_t)this))
		return Socket;
	
	closesocket(Socket);

	Socket = INVALID_SOCKET;

	return Socket;
}
LPFN_ACCEPTEX Network::InitializeAcceptEx(SOCKET Socket)
{
	GUID GuidAcceptEx = WSAID_ACCEPTEX;
	uint32_t Returned = 0;
	LPFN_ACCEPTEX AcceptExPointer;


	WSAIoctl(Socket,
		SIO_GET_EXTENSION_FUNCTION_POINTER,
		&GuidAcceptEx,
		sizeof(GUID),
		&AcceptExPointer,
		sizeof(GuidAcceptEx),
		(PULONG)&Returned,
		0,
		0);

	return AcceptExPointer;
}
LPFN_GETACCEPTEXSOCKADDRS Network::InitializeGetacceptExSockAddrs(SOCKET Socket)
{
	GUID GUIDGetAcceptExSockaddrs = WSAID_GETACCEPTEXSOCKADDRS;
	uint32_t Returned = 0;
	LPFN_GETACCEPTEXSOCKADDRS GetAcceptExSockAddrsPointer;

	WSAIoctl(Socket,
		SIO_GET_EXTENSION_FUNCTION_POINTER,
		&GUIDGetAcceptExSockaddrs,
		sizeof(GUID),
		&GetAcceptExSockAddrsPointer,
		sizeof(GUIDGetAcceptExSockaddrs),
		(PULONG)&Returned,
		0,
		0);

	return GetAcceptExSockAddrsPointer;
}
LPFN_CONNECTEX Network::InitializeConnectEx(SOCKET Socket)
{
	GUID GUIDConnectEx = WSAID_CONNECTEX;
	uint32_t Returned = 0;
	LPFN_CONNECTEX ConnectExPointer;

	WSAIoctl(Socket,
		SIO_GET_EXTENSION_FUNCTION_POINTER,
		&GUIDConnectEx,
		sizeof(GUID),
		&ConnectExPointer,
		sizeof(GUIDConnectEx),
		(PULONG)&Returned,
		0,
		0);

	return ConnectExPointer;
}
HANDLE Network::AssociateCompletionPort(HANDLE Port,HANDLE ExistingPort,uint64_t Key)
{
	HANDLE CompletionPort = CreateIoCompletionPort(Port,ExistingPort,Key,0);

	return CompletionPort;
}

int8_t Network::CreateCompletionThreads()
{
	ULONG Index = 0;
	for(; Index < sizeof(CompletionThreads) / sizeof(ThreadBlock); ++Index)
	{
		CompletionThreads[Index].ThreadHandle = CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)CompletionThread,this,0,&CompletionThreads[Index].ThreadId);
		if(!CompletionThreads[Index].ThreadHandle)
			return 0;
	}
	return 1;
}

int8_t Network::Accept(SOCKET Socket, NetworkAcceptExCompletionRoutine CompletionRoutine,uint64_t ExtraForCallback)
{
	uint32_t Redundant = 0;

	SOCKET IncomingSocket = INVALID_SOCKET;

	LPFN_ACCEPTEX AcceptExPointer = NULL;

	OverlappedExtended* Overlapped = NULL;

	IncomingSocket = CreateSocket();

	if(IncomingSocket == INVALID_SOCKET)
		return 0;


	Overlapped = new OverlappedExtended(
		Socket,
		this,
		OverlappedExtended::OverlappedOperation::Accept,
		NULL,
		0,
		CompletionRoutine,
		(uint32_t)IncomingSocket,
		ExtraForCallback,
		0);

	if(!Overlapped)
		goto CleanUp;

	AcceptExPointer = InitializeAcceptEx(Socket);

	if(!AcceptExPointer)
		goto CleanUp;

	AcceptEx(Socket,
		IncomingSocket,
		NULL,
		0,
		0,
		0,
		(PULONG)&Redundant,
		(LPOVERLAPPED)Overlapped);

	return 1;


CleanUp:

	if(IncomingSocket != INVALID_SOCKET)
	{
		closesocket(IncomingSocket);
		IncomingSocket = INVALID_SOCKET;
	}

	if(Overlapped)
	{
		delete Overlapped;
		Overlapped = NULL;
	}

	return 0;
}
VOID Network::DestroyCompletionTrheads()
{
	ULONG Index = 0;
	for(; Index < sizeof(CompletionThreads) / sizeof(ThreadBlock); ++Index)
	{
		if(CompletionThreads[Index].ThreadId)
		{
			TerminateThread(CompletionThreads[Index].ThreadHandle,0);
			CloseHandle(CompletionThreads[Index].ThreadHandle);
		}
	}

}
HANDLE Network::GetCompletionPort()
{
	return CompletionPort;
}
VOID Network::HandleAcceptInternal(uint64_t Key,OverlappedExtended* Overlapped,uint32_t SizeOfCompletion)
{
	(VOID)Key;
	SOCKET AcceptSocket = Overlapped->Socket;
	SOCKET IncomingSocket = (SOCKET)Overlapped->InternalExtra;

	SOCKADDR AddressLocal = {0};
	int32_t AddressLocalSize = sizeof(SOCKADDR);

	SOCKADDR AddressRemote = {0};
	int32_t AddressRemoteSize = sizeof(SOCKADDR);

	NetworkAcceptExCompletionRoutine CompletionRoutine = (NetworkAcceptExCompletionRoutine)Overlapped->CompletionRoutine;

	(VOID)SizeOfCompletion;

	setsockopt(AcceptSocket,SOL_SOCKET,SO_UPDATE_ACCEPT_CONTEXT,(const char*)&IncomingSocket,sizeof(SOCKET));


	getsockname(IncomingSocket,&AddressLocal,&AddressLocalSize);

	getpeername(IncomingSocket,&AddressRemote,&AddressRemoteSize);


	CompletionRoutine(IncomingSocket,&AddressLocal,AddressLocalSize,&AddressRemote,AddressRemoteSize,Overlapped->ExternalExtra);

	delete Overlapped;
}

int8_t Network::Read(SOCKET Socket,uint8_t* Buffer,uint32_t SizeToRead,NetworkReadWriteCompletionRoutine CompletionRoutine,uint64_t ExtraForCallback,int8_t CompleteOnAll)
{
	OverlappedExtended* Overlapped = new OverlappedExtended(
		Socket,
		this,
		OverlappedExtended::OverlappedOperation::Read,
		Buffer,
		SizeToRead,
		CompletionRoutine,
		0,
		ExtraForCallback,
		CompleteOnAll);

	if(!Overlapped)
		return 0;

	if(ReadWriteInternal(Overlapped))
		return 1;

	delete Overlapped;

	return 0;
}

int8_t Network::Write(SOCKET Socket,uint8_t* Buffer,uint32_t SizeToWrite,NetworkReadWriteCompletionRoutine CompletionRoutine,uint64_t ExtraForCallback,int8_t CompleteOnAll)
{

	OverlappedExtended* Overlapped = new OverlappedExtended(
		Socket,
		this,
		OverlappedExtended::OverlappedOperation::Write,
		Buffer,
		SizeToWrite,
		CompletionRoutine,
		0,
		ExtraForCallback,
		CompleteOnAll);

	if(!Overlapped)
		return 0;


	if(ReadWriteInternal(Overlapped))
		return 1;

	delete Overlapped;

	return 0;
}
int8_t Network::ReadWriteInternal(OverlappedExtended* Overlapped)
{
	WSABUF WsaBuffer = {0};

	WsaBuffer.buf = (char*)Overlapped->Buffer;
	WsaBuffer.len = Overlapped->Size;

	if(ReadWriteInternal(Overlapped,&WsaBuffer))
		return 1;

	return 0;
}
int8_t Network::ReadWriteInternal(OverlappedExtended* Overlapped,LPWSABUF WsaBuffer)
{
	switch(Overlapped->Operation)
	{
	case OverlappedExtended::OverlappedOperation::Read:
		{

			uint32_t Flags = 0;
			if(WSARecv(
				Overlapped->Socket,
				WsaBuffer,
				1,
				NULL,
				(PULONG)&Flags,
				(LPWSAOVERLAPPED)Overlapped,
				NULL) == SOCKET_ERROR)
			{
				if (WSAGetLastError() != WSA_IO_PENDING)
				{
					return 0;
				}
			}
		}
		break;
	case OverlappedExtended::OverlappedOperation::Write:
		{

			if(WSASend(
				Overlapped->Socket,
				WsaBuffer,
				1,
				NULL,
				0,
				(LPWSAOVERLAPPED)Overlapped,
				NULL) == SOCKET_ERROR)
			{
				if (WSAGetLastError() != WSA_IO_PENDING)
				{
					return 0;
				}
			}
		}
		break;
	default:
		return 0;
	}

	return 1;
}
VOID Network::WsaCompletionRoutine(ULONG Error,ULONG Transferred,LPWSAOVERLAPPED Overlapped)
{
	WSABUF WsaBuffer = {0};
	Network* NetworkInstance = NULL;
	NetworkReadWriteCompletionRoutine CompletionRoutine = NULL;
	OverlappedExtended* Extended = (OverlappedExtended*)Overlapped;
	
	CompletionRoutine = (NetworkReadWriteCompletionRoutine)Extended->CompletionRoutine;
	
	Extended->Offset += Transferred;

	if(Error || !Extended->CompleteOnAll || Extended->Offset >= Extended->Size)
	{
		
		CompletionRoutine(Extended->Socket,Error,Extended->Buffer,Extended->Size,Extended->Offset,Extended->ExternalExtra);
		delete Extended;
		return;
	}

	WsaBuffer.buf = (char*)(Extended->Buffer+Extended->Offset);
	WsaBuffer.len = Extended->Size - Extended->Offset;

	NetworkInstance = (Network*)Extended->NetworkInstance;

	Extended->CleanOverlapped();

	if(NetworkInstance->ReadWriteInternal(Extended,&WsaBuffer))
		return;

	CompletionRoutine(Extended->Socket,WSAGetLastError(),Extended->Buffer,Extended->Size,Extended->Offset,Extended->ExternalExtra);


	delete Extended;

}
uint32_t CALLBACK Network::CompletionThread(LPVOID Context)
{
	Network* Self = (Network*)Context;
	while(1)
	{
		OverlappedExtended* Overlapped = NULL;
		uint64_t Key = 0;
		uint32_t SizeOfCompletion = 0;
		int32_t Status = 0;
		
		
		Status = GetQueuedCompletionStatus(Self->GetCompletionPort(),(PULONG)&SizeOfCompletion,&Key,(LPOVERLAPPED*)&Overlapped,INFINITE);

		if(!Overlapped)
			return 0;

		if(!Key)
			return 0;

		if(Overlapped->Operation == OverlappedExtended::OverlappedOperation::Accept)
			Self->HandleAcceptInternal(Key,Overlapped,SizeOfCompletion);
		else
			Self->WsaCompletionRoutine(Status != 0 ? 0 : Status,SizeOfCompletion,Overlapped);



		Sleep(1);

	}
	return 1;

}

int8_t Network::Connect(SOCKET Socket)
{
	if(!connect(Socket,Result->ai_addr,(int32_t)Result->ai_addrlen))
		return 1;

	return 0;
}