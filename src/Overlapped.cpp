#include "..\\include\\Overlapped.h"

OverlappedExtended::OverlappedExtended(/*Connection* Self,*/ SOCKET Socket,
		LPVOID NetworkInstance,
		OverlappedOperation Operation,
		uint8_t* Buffer,
		uint32_t Size,
		LPVOID CompletionRoutine,
		uint64_t InternalExtra,
		uint64_t ExternalExtra,
		int8_t CompleteOnAll)
{
	/*this->Self = Self;*/
	this->Socket = Socket;
	this->NetworkInstance = NetworkInstance;
	this->Operation = Operation;
	this->Buffer = Buffer;
	this->Size = Size;
	this->CompletionRoutine = CompletionRoutine;
	this->InternalExtra = InternalExtra;
	this->ExternalExtra = ExternalExtra;
	this->CompleteOnAll = CompleteOnAll;
	Offset = 0;
	CleanOverlapped();
}
OverlappedExtended::~OverlappedExtended()
{
	/*Self = NULL;*/
	Socket = INVALID_SOCKET;
	NetworkInstance = NULL;
	Operation = (OverlappedOperation)0;
	Buffer = NULL;
	Size = 0;
	CompletionRoutine = NULL;
	InternalExtra = 0;
	ExternalExtra = 0;
	Offset = 0;
	CompleteOnAll = 0;
	CleanOverlapped();

}
LPOVERLAPPED OverlappedExtended::GetOverlapped()
{
	return (LPOVERLAPPED)this;
}
VOID OverlappedExtended::CleanOverlapped()
{
	RtlSecureZeroMemory(this,sizeof(OVERLAPPED));
}